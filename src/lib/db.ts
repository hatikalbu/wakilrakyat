import * as mongoDB from 'mongodb'
import { DB_URI } from '$env/static/private';

const client: mongoDB.MongoClient = new mongoDB.MongoClient(DB_URI);
await client.connect();
const db: mongoDB.Db = client.db('wakilrakyat');

export const getParlimensCollection = async () =>
	(await db.collection('parlimen'))
export const getAccountsCollection = async () =>
	(await db.collection('accounts'))
export const getFeedbacksCollection = async () =>
	(await db.collection('feedbacks'))
export const getPru15sCollection = async () =>
	(await db.collection('pru15'))
export const getBackupsCollection = async () =>
	(await db.collection('backups'))
// export const getCampaignsCollection = async () =>
// 	(await getDb()).collection('campaigns')