import { getPru15sCollection } from '$lib/db'
import { getAccountsCollection } from '$lib/db'
import { ObjectId } from 'mongodb';
import { json, redirect } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

const candidatesCollection = await getPru15sCollection()
const accountsCollection = await getAccountsCollection()


export const GET: RequestHandler = async ({ url, locals, cookies }) => {
	const id = url.searchParams.get('id');
	
	let favourite = locals.user.favourite
	
	if (favourite){
		const unlikeCandidate = await candidatesCollection.updateOne({_id: new ObjectId(favourite)},{
			$inc: {likes: -1}
		}) 
	}

	const updateAccount = await accountsCollection.updateOne({_id: new ObjectId(locals.user.id)},{
		$set: {
			favourite: new ObjectId(id)
		}
	})

	const likeCandidate = await candidatesCollection.updateOne({_id: new ObjectId(id)},{
		$inc: {likes: 1}
	})

	locals.user.favourite = id

	console.log(locals)
	const value = Buffer.from(JSON.stringify(locals.user)).toString('base64');
	cookies.set('session', value, {
		path: '/',
		httpOnly: true,
		maxAge: 60 * 60 * 24 * 3,
	});

  return json({});
}