import { getPru15sCollection } from '$lib/db'
import { getParlimensCollection } from '$lib/db'
import { ObjectId } from 'mongodb';
import type { PageServerLoad, Actions } from './$types';

let candidates = []
let searchP = false
let favourite = []


export const load: PageServerLoad = async ({ locals}) => {
  const user = locals.user
  const candidatesCollection = await getPru15sCollection()
  if (searchP === false){
    candidates = await candidatesCollection.aggregate([{
      $lookup:
        {
          from: "parlimen",
          localField: "code",
          foreignField: "code",
          "pipeline": [
            { "$project": { "_id": { "$toString": "$_id" }, "state": "$state", "code": "$code", "name": "$name" } }
          ],
          as: "code"
        }
    },{
      $group :
          {
            _id : "$code",
            candidate: {$push: {
              _id: {"$toString": "$_id"},
              name:"$name",
              party:"$party",
              component:"$component",
              likes:"$likes"
            }},

          }
    },{ 
      $sort : { _id : 1} 
    }]).toArray();
  }

  searchP = false

  if (user){
    favourite = await candidatesCollection.aggregate([{
      $match: {_id: new ObjectId(locals.user.favourite)}
    },{  
      $lookup:
        {
          from: "parlimen",
          localField: "code",
          foreignField: "code",
          "pipeline": [
            { "$project": { "_id": { "$toString": "$_id" }, "state": "$state", "code": "$code", "name": "$name" } }
          ],
          as: "code"
        }
    },{
      $project: {_id: { "$toString": "$_id" }, name: "$name", component: "$component", party: "$party", likes: "$likes", code: "$code" }
    }]).toArray();
  }

  const candidateLikes = await candidatesCollection.aggregate([{
    $match: {
      likes: {$gte: 1}
    }
  },{  
    $lookup:
      {
        from: "parlimen",
        localField: "code",
        foreignField: "code",
        "pipeline": [
          { "$project": { "_id": { "$toString": "$_id" }, "state": "$state", "code": "$code", "name": "$name" } }
        ],
        as: "code"
      }
  },{
    $project: {_id: { "$toString": "$_id" }, name: "$name", component: "$component", party: "$party", likes: "$likes", code: "$code" }
  },{ 
    $sort : { likes : -1} 
  },{
    $limit: 6
  }]).toArray();

  return {candidates, user, favourite, candidateLikes}
}

export const actions: Actions = {
  default: async (event) => {
    const data = await event.request.formData();
    const search = data.get('search');
    searchP = true
    const parlimensCollection = await getParlimensCollection()
    const parlimens = await parlimensCollection.find({$text: { $search: search }}).project({_id: 0}).sort( { code : 1 } ).toArray();
    
    const newArray = parlimens.map(element => element.code);

    const candidatesCollection = await getPru15sCollection()
    candidates = await candidatesCollection.aggregate([{
      $match:
        { code:{ $in: newArray} }   
    },{
      $lookup:
        {
          from: "parlimen",
          localField: "code",
          foreignField: "code",
          "pipeline": [
            { "$project": { "_id": { "$toString": "$_id" }, "state": "$state", "code": "$code", "name": "$name" } }
          ],
          as: "code"
        }
    },{
      $group :
          {
            _id : "$code",
            candidate: {$push: {
              _id: {"$toString": "$_id"},
              name:"$name",
              party:"$party",
              component:"$component",
              likes:"$likes"
            }},
  
          }
    },{ 
      $sort : { _id : 1} 
    }]).toArray();

    return { success: false }
  }
};