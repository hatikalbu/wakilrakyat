import { redirect } from '@sveltejs/kit'
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ cookies }) => {
	// redirect to `/` if not logged in
	const session = cookies.get('session')
	if (!session) {
		throw redirect(302, '/')
	}

	// eat the cookie
	cookies.set('session', '', {
		path: '/',
		expires: new Date(0),
	})
}