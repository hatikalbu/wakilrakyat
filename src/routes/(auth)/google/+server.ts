import { json } from '@sveltejs/kit';
import { getAccountsCollection } from '$lib/db'
import type { RequestHandler } from './$types';
import jwt_decode from 'jwt-decode'

export const GET: RequestHandler = async ({ url, cookies }) => {
	const jwt = url.searchParams.get('jwt');
	const res = jwt_decode(jwt);
	const accountsCollection = await getAccountsCollection()
	const existingUser = await accountsCollection.findOne({ email: res.email, google: true })
	let payload = {}
	let success = false

	if (!existingUser) {
		const newUser = await accountsCollection.insertOne({ 
			email: res.email,
			name: res.name,
			date_join: new Date(),
			admin: false,
			google: true
		})

		const id = newUser.insertedId.toString()

		payload = {
			email: res.email,
			name: res.name,
			id: id,
			admin: false
		}

		success = true

	} else {
		payload = {
			email: existingUser.email,
			name: existingUser.name,
			id: existingUser._id,
			favourite: existingUser.favourite,
			admin: existingUser.admin
		}

		success = true
	}

	const value = Buffer.from(JSON.stringify(payload)).toString('base64');
	cookies.set('session', value, {
		path: '/',
		httpOnly: true,
		maxAge: 60 * 60 * 24 * 3,
	});

	return json({success: success})
}