import { getAccountsCollection } from '$lib/db'
import type { RequestHandler } from './$types';
import { json } from '@sveltejs/kit';

export const GET: RequestHandler = async ({url, request, cookies}) =>{

	const urlData = url.searchParams.get('data');
	const data = JSON.parse(urlData)
	const accountsCollection = await getAccountsCollection()
	const existingUser = await accountsCollection.findOne({ email: data.email, facebook: true })
	let payload = {}
	let success = false

	if (!existingUser) {
		const newUser = await accountsCollection.insertOne({ 
			email: data.email,
			name: data.name,
			date_join: new Date(),
			facebook_id: data.id,
			facebook: true,
			admin: false
		})

		const id = newUser.insertedId.toString()

		payload = {
			email: data.email,
			name: data.name,
			id: id,
			admin: false
		}

		success = true

	} else {
		payload = {
			email: existingUser.email,
			name: existingUser.name,
			id: existingUser._id,
			favourite: existingUser.favourite,
			admin: false
		}

		success = true
	}

	const value = Buffer.from(JSON.stringify(payload)).toString('base64');
	cookies.set('session', value, {
		path: '/',
		httpOnly: true,
		maxAge: 60 * 60 * 24 * 3,
	});

	return json({success: success})
}