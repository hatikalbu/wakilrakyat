import { getParlimensCollection } from '$lib/db'
import { getFeedbacksCollection } from '$lib/db'
import type { PageServerLoad, Actions } from './$types';

let parlimens = []
let searchP = false

export const load: PageServerLoad = async ({ locals}) => {
  const user = locals.user
  
  if (searchP === false){
    const parlimensCollection = await getParlimensCollection()
    parlimens = await parlimensCollection.find({}).project({_id: 0}).sort( { code : 1 } ).toArray();
  }

  searchP = false
  return {parlimens, user}
}
 
export const actions: Actions = {
  feedback: async (event) => {
    const data = await event.request.formData();
    const name = data.get('name');
    const email = data.get('email');
    const message = data.get('message');

    const feedbacksCollection = await getFeedbacksCollection()
    const newFeedback = await feedbacksCollection.insertOne({ 
			email: email,
			name: name,
      feedback: message,
			date: new Date()
		})

    return { success: true }
  },
  search: async (event) => {
    const data = await event.request.formData();
    const search = data.get('search');
    searchP = true
    const parlimensCollection = await getParlimensCollection()
    parlimens = await parlimensCollection.find({$text: { $search: search }}).project({_id: 0}).sort( { code : 1 } ).toArray();

    return { success: false }
  }
};