import type { Actions } from './$types';
import { getAccountsCollection } from '$lib/db'
import { getBackupsCollection } from '$lib/db'
import { redirect } from '@sveltejs/kit';

export const actions: Actions = {
  default: async (event) => {
    const data = await event.request.formData();
    const email = data.get('email');

		const session = event.cookies.get('session')
		event.cookies.set('session', '', {
			path: '/',
			expires: new Date(0),
		})
		const accountsCollection = await getAccountsCollection()
		const removeAccount = await  accountsCollection.findOneAndDelete({email: email})
		if(removeAccount.value){
			const backupsCollection = await getBackupsCollection()
			const addBackup = await backupsCollection.insertOne(removeAccount.value)
		}
		
		throw redirect(307, '/');
  }
};