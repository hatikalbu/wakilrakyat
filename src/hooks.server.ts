import type { Handle } from '@sveltejs/kit';
 
export const handle: Handle = async ({ event, resolve }) => {
  const session = event.cookies.get('session')
	if (session){
		const value = Buffer.from(session, 'base64').toString('ascii')
		const user = JSON.parse(value)
		event.locals.user = user
	}
 
  const response = await resolve(event);
  return response;
}