import adapter from '@sveltejs/adapter-auto';
import preprocess from 'svelte-preprocess';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess({
		postcss: true,
	}),

	kit: {
		adapter: adapter()
	},
	csp: {
		directives: {
			'script-src': ['https://accounts.google.com/gsi/client', 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v15.0&appId=1879403595738267&autoLogAppEvents=1'], 
			'frame-src': ['https://accounts.google.com/gsi/'], 
			'connect-src': ['https://accounts.google.com/gsi/'],
			'style-src': ['https://accounts.google.com/gsi/style']
		}
	}
};

export default config;
